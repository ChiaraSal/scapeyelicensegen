#include "keygeneratorsvl.h"


KeyGeneratorSVL::KeyGeneratorSVL()
{

}



std::vector<std::string> KeyGeneratorSVL::findAllHardwareIDs(QString PathToFolder)
{
    std::vector<std::string> HWIDs;
    std::vector<QString> Directories;
    QDirIterator it(PathToFolder, QDirIterator::Subdirectories);
    while (it.hasNext())
    {
        QFile f(it.next());
        f.open(QIODevice::ReadOnly);

        QString tempStr = f.fileName();
        QString subString = tempStr.right(4);

        if (subString == ".txt")
        {
            Directories.push_back(tempStr);

            ifstream pathFile(tempStr.toLocal8Bit().constData());
            string content;
            getline(pathFile, content);
            getline(pathFile, content);
            getline(pathFile, content);

            HWIDs.push_back(content);

        }
    }



    return HWIDs;
}





 std::string KeyGeneratorSVL::EasyCrypt(std::string input)
 {
    std::stringstream ss;
    for(unsigned int j = 0; j < input.length(); j++)
    {
        if (input.at(j) == '0')
            ss<<"k";
        else if (input.at(j) == '1')
            ss<<"+";
        else if (input.at(j) == '2')
            ss<<"3";
        else if (input.at(j) == '3')
            ss<<"y";
        else if (input.at(j) == '4')
            ss<<"X";
        else if (input.at(j) == '5')
            ss<<"-";
        else if (input.at(j) == '6')
            ss<<"q";
        else if (input.at(j) == '7')
            ss<<"Q";
        else if (input.at(j) == '8')
            ss<<"0";
        else if (input.at(j) == '9')
            ss<<"&";
    }

    return ss.str();
 }

std::string KeyGeneratorSVL::EasyCombine(std::string HWID, std::string Date)
{
    std::stringstream ss;
    ss<<HWID.substr(0, 6) << Date.substr(0,1)
      <<HWID.substr(6, 6) << Date.substr(1,1)
      <<HWID.substr(12, 6) << Date.substr(2,1)
      <<HWID.substr(18, 2) << Date.substr(3,1)
      <<HWID.substr(20, 10) << Date.substr(4,1)
      <<HWID.substr(30, 1) << Date.substr(5,1)
      <<HWID.substr(31, 6) << Date.substr(6,1)
      <<HWID.substr(37, 3) << Date.substr(7,1)
      <<HWID.substr(40, 5) << Date.substr(8,1)
      <<HWID.substr(45, 6) << Date.substr(9,1)
      <<HWID.substr(51, 3) << Date.substr(10,1)
      <<HWID.substr(54, 6) << Date.substr(11,1)
      <<HWID.substr(60, 1) << Date.substr(12,1)
      <<HWID.substr(61, 3);

    return ss.str();
}

std::string KeyGeneratorSVL::GenerateKey(std::string HWID, std::string StartDate, std::string EndDate)
{

    std::string Final = HWID;
    std::string Date = "";

    Final.append("~TbT5Kp; LeTZ6&>NvmaFgHmTAz7 % 6(WbP!TFlPM8*fE1ZL ? += W6(CvThJLAAtoTw7m!M> / (7Fm * MngY8Tm#ZLrNmQiLaLEY&^mQ~");
    Date.append(StartDate);
    Date.append(EndDate);
    SHA256 sha256;
    std::string HWID_key = sha256(Final);
    std::string Date_key = EasyCrypt(Date);
    std::string Lic = EasyCombine(HWID_key, Date_key);

    return Lic;
    //return HWID_key;

}

std::string KeyGeneratorSVL::GenerateKey_SCL(std::string HWID)
{
    std::string Final = HWID;

    Final.append("~QbT5Kp;DeQZ6&>NvSaFgHsqAz4%6(WbP!qFlPM8*fE1ZL?+=W6(CvThJdAAtoqw4S!M>/(4Fs*MngY8Ts#ZDrNmUiDaDEY&^sU~");
    SHA256 sha256;

    return sha256(Final);

}
