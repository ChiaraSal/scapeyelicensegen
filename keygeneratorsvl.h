#ifndef KEYGENERATORSVL_H
#define KEYGENERATORSVL_H

#include "stdafx.h"


class KeyGeneratorSVL
{
public:
    KeyGeneratorSVL();
    std::string GenerateKey(std::string HWID, std::string StartDate, std::string EndDate);
    std::string GenerateKey_SCL(std::string HWID);
    std::string EasyCrypt(std::string);
    std::string EasyCombine(std::string HWID, std::string Date);
    std::vector<std::string> findAllHardwareIDs(QString PathToFolder);
};

#endif // KEYGENERATORSVL_H
