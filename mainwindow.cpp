#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{

    QString temp = ui->HardwareID->text();
    QDate QStartDate = ui->calendar_startDate->selectedDate();
    QDate QEndDate = ui->calendarEndDate->selectedDate();

    std::string StartDate ="";
    std::string EndDate = "";
    std::stringstream ss_start, ss_end;

    if (QStartDate.month() < 10)
    {
        ss_start<<"0"<< QStartDate.month() << QStartDate.year();
    }
    else
    {
        ss_start<< QStartDate.month() << QStartDate.year();
    }

    StartDate = ss_start.str();

    if (QEndDate.month() < 10)
    {
        ss_end<<"0"<< QEndDate.month() << QEndDate.year();
    }
    else
    {
        ss_end<< QEndDate.month() << QEndDate.year();
    }
    EndDate = ss_end.str();

    std::string HWID = temp.toUtf8().constData();

    KeyGeneratorSVL *KeyGen = new KeyGeneratorSVL();
    std::string licenseKey = KeyGen->GenerateKey(HWID, StartDate, EndDate);
    QString str_licenseKey = QString::fromStdString(licenseKey);

    ui->LicenseKey->setText(str_licenseKey);
}

void MainWindow::on_GenerateKeySCL_clicked()
{
    QString temp = ui->HardwareID->text();
    std::string HWID = temp.toUtf8().constData();
    KeyGeneratorSVL *KeyGen = new KeyGeneratorSVL();
    std::string licenseKey = KeyGen->GenerateKey_SCL(HWID);
    QString str_licenseKey = QString::fromStdString(licenseKey);
    ui->LicenseKey->setText(str_licenseKey);
}



void MainWindow::on_actionOutput_All_SVL_triggered()
{
    QString temp = ui->edit_PathToHW_ID->text();
    QDate QStartDate = ui->calendar_startDate->selectedDate();
    QDate QEndDate = ui->calendarEndDate->selectedDate();

    std::string StartDate ="";
    std::string EndDate = "";
    std::stringstream ss_start, ss_end;

    if (QStartDate.month() < 10)
    {
        ss_start<<"0"<< QStartDate.month() << QStartDate.year();
    }
    StartDate = ss_start.str();

    if (QEndDate.month() < 10)
    {
        ss_end<<"0"<< QEndDate.month() << QEndDate.year();
    }
    EndDate = ss_end.str();

    //std::string HWID = temp.toUtf8().constData();

    KeyGeneratorSVL *KeyGen = new KeyGeneratorSVL();
    vector<string> HWID = KeyGen->findAllHardwareIDs(temp);
    QString pathToOut = ui->edit_PathToOutput->text();
    string outputPath = pathToOut.toLocal8Bit().constData();

    for (int i = 0; i < HWID.size(); i++)
        {
            std::string licenseKey = KeyGen->GenerateKey(HWID[i], StartDate, EndDate);
            QString str_licenseKey = QString::fromStdString(licenseKey);

            // Create folder
            string pathToFile = outputPath + "\\" + HWID[i] + "\\";
            string filename = pathToFile + "License.txt";

            const char * c = pathToFile.c_str();
            _mkdir(c);

            ofstream outputwriter(filename.c_str());

            // Create license
            string content = "LICENSE = \n{\n\tsKey = \"" + licenseKey + "\";\n};";
            outputwriter << content << std::endl ;
            outputwriter.flush();
            outputwriter.close();
        }

    ui->LicenseKey->setText("Multiple licenses");
}

void MainWindow::on_actionOutput_All_SCL_triggered()
{
    QString temp = ui->edit_PathToHW_ID->text();

    KeyGeneratorSVL *KeyGen = new KeyGeneratorSVL();
    vector<string> HWID = KeyGen->findAllHardwareIDs(temp);
    QString pathToOut = ui->edit_PathToOutput->text();
    string outputPath = pathToOut.toLocal8Bit().constData();

    for (int i = 0; i < HWID.size(); i++)
        {
            std::string licenseKey = KeyGen->GenerateKey_SCL(HWID[i]);
            QString str_licenseKey = QString::fromStdString(licenseKey);

            // Create folder
            string pathToFile = outputPath + "\\" + HWID[i] + "\\";
            string filename = pathToFile + "SC_license.txt";

            const char * c = pathToFile.c_str();
            _mkdir(c);

            ofstream outputwriter(filename.c_str());

            // Create license
            string content = licenseKey;
            outputwriter << content << std::endl ;
            outputwriter.flush();
            outputwriter.close();
        }

    ui->LicenseKey->setText("Multiple licenses");
}
