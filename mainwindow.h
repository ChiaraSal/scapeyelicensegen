#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "keygeneratorsvl.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_pushButton_clicked();

    void on_GenerateKeySCL_clicked();

    void on_actionOutput_All_SVL_triggered();

    void on_actionOutput_All_SCL_triggered();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
